<?php

namespace Drupal\on_page_help\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implemetnation of the 'route_autocomplete_textfield' widget.
 */
#[FieldWidget(
  id: 'route_autocomplete_textfield',
  label: new TranslatableMarkup('Route Autocomplete Textfield'),
  field_types: ['string']
)]
class RouteAutocompleteTextfieldWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['value'] += [
      '#autocomplete_route_name' => 'on_page_help.autocomplete.route',
    ];

    return $element;
  }

}
