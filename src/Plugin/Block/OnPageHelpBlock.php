<?php

namespace Drupal\on_page_help\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'OnPageHelpBlock' block.
 *
 * @Block(
 *  id = "on_page_help_block",
 *  admin_label = @Translation("On-Page Help block"),
 * )
 */
class OnPageHelpBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The OnPageHelpEntityStorage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $onPageHelpEntityStorage;

  /**
   * The OnPageHelpTypeEntityStorage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $onPageHelpTypeEntityStorage;

  /**
   * The current route.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Current user (proxy object).
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Build an OnPageHelpBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $on_page_help_entity_storage
   *   An OnPageHelpEntityStorage object.
   * @param \Drupal\Core\Entity\EntityStorageInterface $on_page_help_type_entity_storage
   *   An OnPageHelpTypeEntityStorage object.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   A CurrentRouteMatch object describing the current page.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A proxy object to the current user.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   An object containing the current path.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityStorageInterface $on_page_help_entity_storage,
    EntityStorageInterface $on_page_help_type_entity_storage,
    CurrentRouteMatch $route_match,
    AccountProxyInterface $current_user,
    CurrentPathStack $current_path,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->onPageHelpEntityStorage = $on_page_help_entity_storage;
    $this->onPageHelpTypeEntityStorage = $on_page_help_type_entity_storage;
    $this->routeMatch = $route_match;
    $this->currentUser = $current_user;
    $this->currentPath = $current_path;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('on_page_help'),
      $container->get('entity_type.manager')->getStorage('on_page_help_type'),
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('path.current')
    );
  }

  /**
   * Fetch the entity for the given route accessible by the current user.
   *
   * @param string $route
   *   A route name.  E.g., entity.node.canonical.
   * @param string $on_page_help_type
   *   The bundle for this on page help item.
   *
   * @return \Drupal\on_page_help\Entity\OnPageHelpEntity|null
   *   The OnPageHelpEntity for this page and user, or NULL if none.
   */
  protected function getHelpEntityForRoute(string $route, string $on_page_help_type) {
    $ids = $this->onPageHelpEntityStorage->idsMatchingRoute($route, $on_page_help_type, $this->currentUser->hasPermission('view unpublished on-page help'));
    $url_object = Url::fromUserInput($this->currentPath->getPath());
    $access = $url_object->access($this->currentUser);
    // Check if user has access to the current path.
    if (!$access) {
      return NULL;
    }
    // Nothing to load, return NULL.
    if (empty($ids)) {
      return NULL;
    }

    // Load the help items and find the first one the current user can see.
    $help_items = $this->onPageHelpEntityStorage->loadMultiple($ids);
    $account_roles = $this->currentUser->getRoles();
    $node = $this->routeMatch->getParameter('node');

    // Find the first help_item that matches all the criteria.
    foreach ($help_items as $help_item) {
      // If this is a node-page, and the help_item has node_type restrictions,
      // check that the current node is one of the required node types.
      if ($node) {
        $valid_nodetypes = array_column($help_item->get('node_types')->getValue(), 'value');
        if ($valid_nodetypes && !in_array($node->getType(), $valid_nodetypes)) {
          continue;
        }
      }

      // If the help item has role restrictions, check the user has ALL the
      // roles in the help item.
      $item_roles = array_column($help_item->get('roles')->getValue(), 'value');
      if ($item_roles) {
        $role_intersect = array_intersect($account_roles, $item_roles);
        if (count($item_roles) != count($role_intersect)) {
          continue;
        }
      }

      // Verify the user has permission to view the help item.
      $access = $help_item->access('view');
      if ($access) {
        return $help_item;
      }
    }

    // Nothing passed, return NULL.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_name = $this->routeMatch->getRouteName();
    if (!$route_name) {
      return [];
    }
    $on_page_help_type = $this->getConfiguration()['on_page_help_type'];
    $item = $this->getHelpEntityForRoute($route_name, $on_page_help_type);
    $can_add_link = $this->currentUser->hasPermission('add on-page help') ||
      $this->currentUser->hasPermission('administer on-page help');

    // If there's an OPH for the route.
    if ($item) {
      return [
        '#theme' => 'on_page_help_block',
        '#on_page_help' => $item,
        '#block_on_page_help_type' => $on_page_help_type,
        '#contextual_links' => [
          'on_page_help_add' => [
            'route_parameters' => [
              'on_page_help_type' => $on_page_help_type,
            ] + $this->getAddLinkOptions($route_name),
          ],
          'on_page_help_edit' => [
            'route_parameters' => [
              'on_page_help' => $item->id(),
            ],
          ],
        ],
        '#attributes' => [
          'class' => [$item->isPublished() ? 'published' : 'unpublished'],
        ],
      ];
    }
    // If there's no OPH but user has permission to add a link.
    elseif ($can_add_link) {
      return [
        '#theme' => 'on_page_help_block',
        '#new_oph_link' => $this->getAddLink($route_name, $on_page_help_type),
        '#block_on_page_help_type' => $on_page_help_type,
        '#contextual_links' => [
          'on_page_help_add' => [
            'route_parameters' => [
              'on_page_help_type' => $on_page_help_type,
            ] + $this->getAddLinkOptions($route_name),
          ],
        ],
        '#attributes' => ['class' => ['unpublished', 'default-on-page-help']],
      ];
    }
    // Return empty.
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path', 'user']);
  }

  /**
   * Helper function to return link to create new OPH.
   *
   * @param string $route
   *   Current route.
   * @param string $on_page_help_type
   *   The bundle of the on page help item.
   *
   * @return array
   *   Return link to create new OPH with route/CT filled in.
   */
  private function getAddLink(string $route, string $on_page_help_type) {
    $url = Url::fromRoute('entity.on_page_help.add_form', ['on_page_help_type' => $on_page_help_type]);
    $url->setOptions(['query' => $this->getAddLinkOptions($route)]);
    return Link::fromTextAndUrl($this->t('Add a new on-page help'), $url)->toRenderable();
  }

  /**
   * Get the query for an add OPH link.
   *
   * @param string $route
   *   Current route.
   *
   * @return array
   *   A list of options for the add OPH link, including the route & CT.
   */
  private function getAddLinkOptions(string $route) {
    $options['edit[route][widget][0][value]'] = $route;

    $node = $this->routeMatch->getParameter('node');
    if ($node instanceof NodeInterface) {
      $options['edit[node_types][widget]'] = $node->bundle();
    }
    $options['destination'] = Url::fromRoute('<current>')->toString();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'on_page_help_type' => 'route_on_page_help',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;
    $defaults = $this->defaultConfiguration();
    $oph_type_ids = $this->onPageHelpTypeEntityStorage->getQuery()->accessCheck()->execute();
    $opy_type_options = [];
    if (!empty($oph_type_ids)) {
      $oph_types = $this->onPageHelpTypeEntityStorage->loadMultiple($oph_type_ids);
      foreach ($oph_types as $oph_type) {
        $opy_type_options[$oph_type->id()] = $oph_type->label();
      }
    }
    $destination = Url::fromRoute('<current>')->toString();
    $oph_type_url = Url::fromRoute('entity.on_page_help_type.collection', [], ['query' => ['destination' => $destination]])->toString();
    if (empty($opy_type_options)) {
      $description = $this->t('At least one <a href=":oph_type_url">type of OPH</a> must be created first. Different types of OPH are completely separate and will use a separate block to display their help items.', [':oph_type_url' => $oph_type_url]);
    }
    else {
      $description = $this->t('Different types of OPH are completely separate and will use separate blocks to display their help items. Do you need to create a new <a href=":oph_type_url">OPH type</a>?', [':oph_type_url' => $oph_type_url]);
    }

    $form['on_page_help_type'] = [
      '#type' => 'select',
      '#title' => $this->t('On-Page Help Type'),
      '#description' => $description,
      '#options' => $opy_type_options,
      '#required' => TRUE,
      '#default_value' => $config['on_page_help_type'] ?? $defaults['on_page_help_type'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['on_page_help_type'] = $form_state->getValue('on_page_help_type');
  }

}
