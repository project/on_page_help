<?php

namespace Drupal\on_page_help;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of On-page Help entities.
 *
 * @ingroup on_page_help
 */
class OnPageHelpEntityListBuilder extends EntityListBuilder {

  /**
   * The OPH type config entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $ophtStorage;

  /**
   * Constructs a new MediaListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorage $opht_storage
   *   The OPH type config entity storage.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ConfigEntityStorage $opht_storage) {
    parent::__construct($entity_type, $storage);

    $this->ophtStorage = $opht_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $entity_type_manager->getStorage('on_page_help_type'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo Build sorting and filtering (by type) into this form:
   * https://drupal.stackexchange.com/questions/255724/how-to-create-custom-search-filter-for-entity-list.
   */
  public function buildHeader() {
    $header['on_page_help_type'] = $this->t('On-Page Help Type');
    $header['name'] = $this->t('Name');
    $header['route'] = $this->t('Route(s)');
    $header['link_text'] = $this->t('Help Link Text');
    $header['link_uri'] = $this->t('Help Link URI');
    $header['roles'] = $this->t('Roles');
    $header['content_types'] = $this->t('Content Types');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['on_page_help_type'] = $this->ophtStorage->load($entity->bundle())->label();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.on_page_help.edit_form',
      ['on_page_help' => $entity->id()]
    );
    $row['route'] = $entity->get('route')->getString();
    $row['link_text'] = '';
    $row['link_uri'] = '';
    if (!$entity->get('help_link')->isEmpty()) {
      $help_link = $entity->get('help_link')->first()->getValue();
      $uri = Url::fromUri($help_link['uri']);
      // Title is optional and will be the uri if not given.
      if (empty($help_link['title'])) {
        $help_link['title'] = $uri->toString();
      }
      $row['link_text'] = Link::fromTextAndUrl($help_link['title'], Url::fromUri($help_link['uri']));
      $row['link_uri'] = $help_link['uri'];
    }
    $row['roles'] = $entity->get('roles')->getString();
    $row['content_types'] = $entity->get('node_types')->getString();
    $row['weight'] = $entity->get('weight')->getString();
    return $row + parent::buildRow($entity);
  }

}
