<?php

namespace Drupal\on_page_help\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for On-page Help entities.
 */
class OnPageHelpEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
