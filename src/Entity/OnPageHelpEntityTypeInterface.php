<?php

namespace Drupal\on_page_help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining On-page Help entity type entities.
 */
interface OnPageHelpEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
