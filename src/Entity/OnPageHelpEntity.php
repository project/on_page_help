<?php

namespace Drupal\on_page_help\Entity;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Defines the On-page Help entity.
 *
 * @ingroup on_page_help
 *
 * @ContentEntityType(
 *   id = "on_page_help",
 *   label = @Translation("On-page Help"),
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_label = @Translation("On-page Help type"),
 *   handlers = {
 *     "storage" = "Drupal\on_page_help\OnPageHelpEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\on_page_help\OnPageHelpEntityListBuilder",
 *     "views_data" = "Drupal\on_page_help\Entity\OnPageHelpEntityViewsData",
 *     "translation" = "Drupal\on_page_help\OnPageHelpEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\on_page_help\Form\OnPageHelpEntityForm",
 *       "add" = "Drupal\on_page_help\Form\OnPageHelpEntityForm",
 *       "edit" = "Drupal\on_page_help\Form\OnPageHelpEntityForm",
 *       "delete" = "Drupal\on_page_help\Form\OnPageHelpEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\on_page_help\OnPageHelpEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\on_page_help\OnPageHelpEntityAccessControlHandler",
 *   },
 *   base_table = "on_page_help",
 *   data_table = "on_page_help_field_data",
 *   revision_table = "on_page_help_revision",
 *   revision_data_table = "on_page_help_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer on-page help",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/on_page_help/{on_page_help}",
 *     "add-page" = "/admin/on_page_help/add",
 *     "add-form" = "/admin/on_page_help/add/{on_page_help_type}",
 *     "edit-form" = "/admin/on_page_help/{on_page_help}/edit",
 *     "delete-form" = "/admin/on_page_help/{on_page_help}/delete",
 *     "version-history" = "/admin/on_page_help/{on_page_help}/revisions",
 *     "revision" = "/admin/on_page_help/{on_page_help}/revisions/{on_page_help_revision}/view",
 *     "revision_revert" = "/admin/on_page_help/{on_page_help}/revisions/{on_page_help_revision}/revert",
 *     "revision_delete" = "/admin/on_page_help/{on_page_help}/revisions/{on_page_help_revision}/delete",
 *     "translation_revert" = "/admin/on_page_help/{on_page_help}/revisions/{on_page_help_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/on_page_help",
 *   },
 *   bundle_entity_type = "on_page_help_type",
 *   field_ui_base_route = "entity.on_page_help_type.edit_form"
 * )
 */
class OnPageHelpEntity extends EditorialContentEntityBase implements OnPageHelpEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the on_page_help owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the On-page Help item.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode']->setDescription(t('The term language code.'));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the On-page Help item.  This is only visible to help editors.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['help_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Help Text'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['help_link'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Help link'))
      ->setDescription(t('The location this help link points to.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'link_default',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this on-page help item in relation to others.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['route'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Routes'))
      ->setDescription(t('The Drupal route(s) to display this On-Page Help. Wildcards allowed.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'route_autocomplete_textfield',
        'weight' => -7,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['roles'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Required Role(s)'))
      ->setDescription(t('User must have ALL selected roles.'))
      ->setSettings([
        'allowed_values' => array_map(fn(RoleInterface $role) => $role->label(), Role::loadMultiple()),
      ])
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -6,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE);

    $fields['node_types'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Content Types'))
      ->setDescription(t('Apply to any selected Content Type.'))
      ->setSettings([
        'allowed_values' => node_type_get_names(),
      ])
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -5,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the On-page Help item is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 10,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the help item was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the help item was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
