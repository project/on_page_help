<?php

namespace Drupal\on_page_help\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining On-page Help entities.
 *
 * @ingroup on_page_help
 */
interface OnPageHelpEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the On-page Help entity name.
   *
   * @return string
   *   Name of the On-page Help entity.
   */
  public function getName();

  /**
   * Sets the On-page Help entity name.
   *
   * @param string $name
   *   The On-page Help entity name.
   *
   * @return \Drupal\on_page_help\Entity\OnPageHelpEntityInterface
   *   The called On-page Help entity.
   */
  public function setName($name);

  /**
   * Gets the On-page Help entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the On-page Help entity.
   */
  public function getCreatedTime();

  /**
   * Sets the On-page Help entity creation timestamp.
   *
   * @param int $timestamp
   *   The On-page Help entity creation timestamp.
   *
   * @return \Drupal\on_page_help\Entity\OnPageHelpEntityInterface
   *   The called On-page Help entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the On-page Help entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the On-page Help entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\on_page_help\Entity\OnPageHelpEntityInterface
   *   The called On-page Help entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the On-page Help entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the On-page Help entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\on_page_help\Entity\OnPageHelpEntityInterface
   *   The called On-page Help entity.
   */
  public function setRevisionUserId($uid);

}
