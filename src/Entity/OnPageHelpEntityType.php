<?php

namespace Drupal\on_page_help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the On-page Help entity type entity.
 *
 * @ConfigEntityType(
 *   id = "on_page_help_type",
 *   label = @Translation("On-page Help type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\on_page_help\OnPageHelpEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\on_page_help\Form\OnPageHelpEntityTypeForm",
 *       "edit" = "Drupal\on_page_help\Form\OnPageHelpEntityTypeForm",
 *       "delete" = "Drupal\on_page_help\Form\OnPageHelpEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\on_page_help\OnPageHelpEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "on_page_help_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "on_page_help",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/on_page_help/{on_page_help_type}",
 *     "add-form" = "/admin/structure/on_page_help/add",
 *     "edit-form" = "/admin/structure/on_page_help/{on_page_help_type}/edit",
 *     "delete-form" = "/admin/structure/on_page_help/{on_page_help_type}/delete",
 *     "collection" = "/admin/structure/on_page_help"
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   }
 * )
 */
class OnPageHelpEntityType extends ConfigEntityBundleBase implements OnPageHelpEntityTypeInterface {

  /**
   * The On-page Help entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The On-page Help entity type label.
   *
   * @var string
   */
  protected $label;

}
