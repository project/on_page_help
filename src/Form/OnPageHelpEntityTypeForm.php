<?php

namespace Drupal\on_page_help\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * An EntityForm for On-page help bundles.
 */
class OnPageHelpEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $on_page_help_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $on_page_help_type->label(),
      '#description' => $this->t("Label for the On-page Help entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $on_page_help_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\on_page_help\Entity\OnPageHelpEntityType::load',
      ],
      '#disabled' => !$on_page_help_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $on_page_help_type = $this->entity;
    $status = $on_page_help_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label On-page Help entity type.', [
          '%label' => $on_page_help_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label On-page Help entity type.', [
          '%label' => $on_page_help_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($on_page_help_type->toUrl('collection'));
  }

}
