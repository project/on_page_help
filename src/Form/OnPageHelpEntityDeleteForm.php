<?php

namespace Drupal\on_page_help\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting On-page Help entities.
 *
 * @ingroup on_page_help
 */
class OnPageHelpEntityDeleteForm extends ContentEntityDeleteForm {


}
