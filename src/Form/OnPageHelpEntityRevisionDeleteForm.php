<?php

namespace Drupal\on_page_help\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a On-page Help entity revision.
 *
 * @ingroup on_page_help
 */
class OnPageHelpEntityRevisionDeleteForm extends ConfirmFormBase {

  use StringTranslationTrait;

  /**
   * The On-page Help entity revision.
   *
   * @var \Drupal\on_page_help\Entity\OnPageHelpEntityInterface
   */
  protected $revision;

  /**
   * The On-page Help entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $onPageHelpEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->onPageHelpEntityStorage = $container->get('entity_type.manager')->getStorage('on_page_help');
    $instance->connection = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'on_page_help_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.on_page_help.version_history', ['on_page_help' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $on_page_help_revision = NULL) {
    $this->revision = $this->onPageHelpEntityStorage->loadRevision($on_page_help_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->onPageHelpEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('On-page Help entity: deleted %title revision %revision.', [
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()->addMessage($this->t('Revision from %revision-date of On-page Help entity %title has been deleted.', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.on_page_help.canonical',
       ['on_page_help' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {on_page_help_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.on_page_help.version_history',
         ['on_page_help' => $this->revision->id()]
      );
    }
  }

}
