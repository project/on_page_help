<?php

namespace Drupal\on_page_help;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\on_page_help\Entity\OnPageHelpEntityInterface;

/**
 * Defines the storage handler class for On-page Help entities.
 *
 * This extends the base storage class, adding required special handling for
 * On-page Help entities.
 *
 * @ingroup on_page_help
 */
interface OnPageHelpEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of OPH entity revision IDs for a specific On-page Help entity.
   *
   * @param \Drupal\on_page_help\Entity\OnPageHelpEntityInterface $entity
   *   The On-page Help entity.
   *
   * @return int[]
   *   On-page Help entity revision IDs (in ascending order).
   */
  public function revisionIds(OnPageHelpEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as OPH entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   On-page Help entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\on_page_help\Entity\OnPageHelpEntityInterface $entity
   *   The On-page Help entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(OnPageHelpEntityInterface $entity);

  /**
   * Unsets the language for all On-page Help entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
