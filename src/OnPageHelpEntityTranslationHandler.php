<?php

namespace Drupal\on_page_help;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for on_page_help.
 */
class OnPageHelpEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
