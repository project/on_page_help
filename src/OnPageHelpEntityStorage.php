<?php

namespace Drupal\on_page_help;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\on_page_help\Entity\OnPageHelpEntityInterface;

/**
 * Defines the storage handler class for On-page Help entities.
 *
 * This extends the base storage class, adding required special handling for
 * On-page Help entities.
 *
 * @ingroup on_page_help
 */
class OnPageHelpEntityStorage extends SqlContentEntityStorage implements OnPageHelpEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(OnPageHelpEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {on_page_help_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {on_page_help_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(OnPageHelpEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {on_page_help_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('on_page_help_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

  /**
   * Return list of On-page Help entity IDs matching a route.
   *
   * @param string $route
   *   The route to check.  E.g., entity.node.canonical.
   * @param string $on_page_help_type
   *   The bundle for this on page help item.
   * @param array $unpublished
   *   Include unpublished content if TRUE.
   *
   * @return array
   *   A list of matching On-page help entity IDs.
   */
  public function idsMatchingRoute(string $route, string $on_page_help_type, bool $unpublished = FALSE) {
    if (!$route) {
      return [];
    }
    $ids = $this->database->query(
      "SELECT DISTINCT entity_id FROM {on_page_help__route} r WHERE :route LIKE REPLACE(r.route_value, '*', '%')",
      [':route' => $route]
    )->fetchcol();

    if (!$ids) {
      return [];
    }

    $query = $this->database->select('on_page_help_field_data', 'oph')
      ->fields('oph', ['id'])
      ->condition('id', $ids, 'IN')
      ->condition('type', $on_page_help_type)
      ->orderBy('weight', 'ASC')
      ->orderBy('status', 'DESC');

    if (!$unpublished) {
      $query->condition('status', 1);
    }

    return $query->execute()->fetchCol();
  }

}
