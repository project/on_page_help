<?php

namespace Drupal\on_page_help\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\on_page_help\Entity\OnPageHelpEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OnPageHelpEntityController.
 *
 *  Returns responses for On-page Help entity routes.
 */
class OnPageHelpEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a On-page Help entity revision.
   *
   * @param int $on_page_help_revision
   *   The On-page Help entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($on_page_help_revision) {
    $on_page_help = $this->entityTypeManager()->getStorage('on_page_help')
      ->loadRevision($on_page_help_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('on_page_help');

    return $view_builder->view($on_page_help);
  }

  /**
   * Page title callback for a On-page Help entity revision.
   *
   * @param int $on_page_help_revision
   *   The On-page Help entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($on_page_help_revision) {
    $on_page_help = $this->entityTypeManager()->getStorage('on_page_help')
      ->loadRevision($on_page_help_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $on_page_help->label(),
      '%date' => $this->dateFormatter->format($on_page_help->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a On-page Help entity.
   *
   * @param \Drupal\on_page_help\Entity\OnPageHelpEntityInterface $on_page_help
   *   A On-page Help entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(OnPageHelpEntityInterface $on_page_help) {
    $account = $this->currentUser();
    $on_page_help_storage = $this->entityTypeManager()->getStorage('on_page_help');

    $langcode = $on_page_help->language()->getId();
    $langname = $on_page_help->language()->getName();
    $languages = $on_page_help->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $on_page_help->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $on_page_help->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all on-page help revisions") || $account->hasPermission('administer on-page help')));
    $delete_permission = (($account->hasPermission("delete all on-page help revisions") || $account->hasPermission('administer on-page help')));

    $rows = [];

    $vids = $on_page_help_storage->revisionIds($on_page_help);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\on_page_help\Entity\OnPageHelpEntityInterface $revision */
      $revision = $on_page_help_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $on_page_help->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.on_page_help.revision', [
            'on_page_help' => $on_page_help->id(),
            'on_page_help_revision' => $vid,
          ]));
        }
        else {
          $link = $on_page_help->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.on_page_help.translation_revert', [
                'on_page_help' => $on_page_help->id(),
                'on_page_help_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.on_page_help.revision_revert', [
                'on_page_help' => $on_page_help->id(),
                'on_page_help_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.on_page_help.revision_delete', [
                'on_page_help' => $on_page_help->id(),
                'on_page_help_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['on_page_help_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
