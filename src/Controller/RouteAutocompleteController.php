<?php

namespace Drupal\on_page_help\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides autocomplete service on the router.
 */
class RouteAutocompleteController extends ControllerBase {

  /**
   * Database object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Router object.
   *
   * @var \Drupal\Core\Routing\Router
   */
  protected $router;

  /**
   * {@inheritdoc}
   */
  public function __construct(Router $router, Connection $database) {
    $this->router = $router;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.no_access_checks'),
      $container->get('database'),
    );
  }

  /**
   * Add paths from Path Alias table.
   */
  protected function addPathAliasMatches(&$results, $search_key) {
    $aliases = $this->database->select('path_alias', 'pa')
      ->fields('pa', ['path', 'alias'])
      ->range(0, 10)
      ->condition('alias', $search_key, 'LIKE')
      ->execute()
      ->fetchAll();

    foreach ($aliases as $alias) {
      $request = Request::create($alias->path);
      $route = $this->router->matchRequest($request);

      if ($route) {
        $results[$route['name']] = [
          'value' => $route['_route'],
          'label' => "{$alias->alias} ({$route['_route']})",
        ];

      }
    }

    return $results;
  }

  /**
   * Add matches from the router.
   */
  protected function addRouterMatches(&$results, $search_key) {
    $query = $this->database->select('router', 'r')
      ->fields('r', ['name', 'path'])
      ->range(0, 10);
    $search_condition = $query->orConditionGroup()
      ->condition('name', $search_key, 'LIKE')
      ->condition('path', $search_key, 'LIKE');
    $routes = $query->condition($search_condition)
      ->execute()
      ->fetchAll();
    foreach ($routes as $route) {
      $results[$route->name] = [
        'value' => $route->name,
        'label' => "{$route->path} ({$route->name})",
      ];
    }
    return $results;
  }

  /**
   * Return a list of matches based on the query.
   */
  public function getMatches(Request $request) {
    $results = [];

    $search_key = $request->query->get('q');

    if ($search_key && $search_key !== '/') {
      // Prefer paths.
      if (strpos($search_key, '/') !== 0) {
        $results = $this->addPathAliasMatches($results, "/$search_key%");
        $results = $this->addRouterMatches($results, "/$search_key%");
      }
      $results = $this->addPathAliasMatches($results, "$search_key%");
      $results = $this->addRouterMatches($results, "$search_key%");
      $results = $this->addPathAliasMatches($results, "%$search_key%");
      $results = $this->addRouterMatches($results, "%$search_key%");
    }

    return new JsonResponse(array_values($results));
  }

}
