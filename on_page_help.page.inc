<?php

/**
 * @file
 * Contains on_page_help.page.inc.
 *
 * Page callback for On-page Help entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for On-page Help templates.
 *
 * Default template: on_page_help.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_on_page_help(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for the On-page help block.
 *
 * Default template: on_page_help_block.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - on_page_help: the On-page Help entity.
 */
function template_preprocess_on_page_help_block(array &$variables) {
  $on_page_help = $variables['on_page_help'];
  if ($on_page_help) {
    if (!$on_page_help->help_link->isEmpty()) {
      $on_page_help->help_link->options += ['attributes' => ['target' => '_blank']];
    }
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('on_page_help');
    $pre_render = $view_builder->view($on_page_help, 'block');
    $variables['content'][] = $pre_render;
  }
}
